export default async function handler(req, res) {
  const { cityId } = req.query;

  let response = new Response();
  try {
    response = await fetch(
      `https://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=f4ea689bc636fcf4c6e9a08ea5f9f7f8`
    );
  } catch (error) {
    res.status(response.status).json({ error: error });
  } finally {
    const hourlyWeatherData = await response.json();

    res.status(response.status).json(hourlyWeatherData);
  }
}
