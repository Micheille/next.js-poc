import { useState } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { signIn, signOut, useSession } from 'next-auth/client';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import styles from '../styles/home.module.css';

export default function Home() {
  const [session, loading] = useSession();
  const [cityId, setCityId] = useState(499099);

  const CityControl = () => (
    <FormControl>
      <InputLabel id='citySelect'>City</InputLabel>
      <Select
        labelId='citySelect'
        id='citySelect'
        value={cityId}
        onChange={(event) => setCityId(event.target.value)}
      >
        <MenuItem value={499099}>Samara</MenuItem>
        <MenuItem value={524894}>Moscow</MenuItem>
        <MenuItem value={498817}>Saint Petersburg</MenuItem>
      </Select>
    </FormControl>
  );

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main>
        <h1 className={styles.title}>Next.js PoC</h1>

        <div className={styles.grid}>
          <div className={styles.card}>
            <h3>Auth &rarr;</h3>

            <div className={styles.signedInStatus}>
              <p>
                {!session && (
                  <>
                    <span className={styles.notSignedInText}>
                      You are not signed in
                    </span>
                    <a
                      href={`/api/auth/signin`}
                      className={styles.buttonPrimary}
                      onClick={(e) => {
                        e.preventDefault();
                        signIn();
                      }}
                    >
                      Sign in
                    </a>
                  </>
                )}
                {session && (
                  <>
                    {session.user.image && (
                      <span
                        style={{
                          backgroundImage: `url(${session.user.image})`,
                        }}
                        className={styles.avatar}
                      />
                    )}
                    <span className={styles.signedInText}>
                      <small>Signed in as</small>
                      <br />
                      <strong>{session.user.email || session.user.name}</strong>
                    </span>
                    <a
                      href={`/api/auth/signout`}
                      className={styles.button}
                      onClick={(e) => {
                        e.preventDefault();
                        signOut();
                      }}
                    >
                      Sign out
                    </a>
                  </>
                )}
              </p>
            </div>
          </div>

          <Link href='/protected'>
            <a className={styles.card}>
              <h3>Protected Page &rarr;</h3>
              <p>You must be signed in to view this page</p>
            </a>
          </Link>

          <div className={styles.card}>
            <h3>SSR &rarr;</h3>
            <p>
              Choose a city and see
              <Link href='/ssr/[cityId]' as={`/ssr/${cityId}`}>
                <a> its current weather</a>
              </Link>
            </p>
            <CityControl />
          </div>

          <div className={styles.card}>
            <h3>API Routes &rarr;</h3>
            <p>
              Same as on the left but with
              <Link href='/api-route/[cityId]' as={`/api-route/${cityId}`}>
                <a> API Route page</a>
              </Link>
            </p>
            <CityControl />
          </div>
        </div>
      </main>

      <footer>
        <a
          href='https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app'
          target='_blank'
          rel='noopener noreferrer'
        >
          Powered by{' '}
          <img src='/vercel.svg' alt='Vercel' className={styles.logo} />
        </a>
      </footer>
    </div>
  );
}
