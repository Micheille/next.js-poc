import { useRouter } from 'next/router';

import Layout from '../../components/layout';

export async function getServerSideProps({ query }) {
  const res = await fetch(
    `https://api.openweathermap.org/data/2.5/weather?id=${query.cityId}&appid=f4ea689bc636fcf4c6e9a08ea5f9f7f8`
  );
  const data = await res.json();

  return { props: { data } };
}

export default function CurrentWeatherSSR({ data }) {
  return (
    <Layout>
      <h2>
        <strong>{data.name}</strong>
      </h2>
      <p>{data.weather[0].main}</p>
      <p>Temperature: {Math.round(data.main.temp - 273)} °C</p>
    </Layout>
  );
}
