import { useRouter } from 'next/router';
import useSWR from 'swr';

import Layout from '../../components/layout';

const fetcher = async (url) => {
  const res = await fetch(url);
  const weatherData = await res.json();
  return weatherData;
};

export default function CurrentWeatherAPIRoute() {
  const router = useRouter();
  const { cityId } = router.query;

  const { data, error } = useSWR(
    cityId ? `/api/weather/${cityId}` : null,
    fetcher
  );

  if (error)
    return (
      <Layout>
        <div>{error.message}</div>
      </Layout>
    );
  if (!data)
    return (
      <Layout>
        <div>Loading...</div>
      </Layout>
    );

  return (
    <Layout>
      <h2>
        <strong>{data.name}</strong>
      </h2>
      <p>{data.weather[0].main}</p>
      <p>Temperature: {Math.round(data.main.temp - 273)} °C</p>
    </Layout>
  );
}
